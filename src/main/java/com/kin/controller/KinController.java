package com.kin.controller;

import com.kin.JDBCDemo;
import com.kin.dao.KinpersonDAO;
import com.kin.dao.exception.KinpersonDAOException;
import com.kin.dao.impl.hibernate.KinpersonDAOHibernateImpl;
import com.kin.dao.impl.jdbc.KinpersonDAOJDBCImpl;
import com.kin.dao.impl.mybatis.KinpersonDAOMybatisImpl;
import com.kin.model.Kinperson;
import com.kin.model.Place;
import com.kin.model.Town;
import com.kin.model.Village;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * Created by PK on 10/7/2015.
 */
@Controller
public class KinController {

    private static Logger LOGGER = LoggerFactory.getLogger(KinController.class);

    @Autowired
    KinpersonDAOMybatisImpl kinpersonDAO;

    @Resource(name="personDAO3")
    KinpersonDAOHibernateImpl kinpersonDAOHibernate;

    @RequestMapping(value="/mybatis", method = RequestMethod.GET)
    public String mybatisTest(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("-------- mybatis ------");
        try {
            LOGGER.info(String.valueOf(kinpersonDAO.findById(1)));
        } catch (KinpersonDAOException e) {
            e.printStackTrace();
        }
        return "operationSuccess";
    }

    @RequestMapping(value="/jdbc", method = RequestMethod.GET)
    public String jdbcTest(HttpServletRequest request, HttpServletResponse response) throws KinpersonDAOException {
        LOGGER.info("-------- jdbc ------");
//        createKinpersons();
        LOGGER.info(String.valueOf(new KinpersonDAOJDBCImpl().findById(1)));
        return "operationSuccess";
    }

    @RequestMapping(value="/hibernate", method = RequestMethod.GET)
    public String hibernateTest(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("-------- hibernate ------");
        try {
            LOGGER.info(String.valueOf(kinpersonDAOHibernate.findById(1)));
        } catch (KinpersonDAOException e) {
            e.printStackTrace();
        }
        return "operationSuccess";
    }

    private static void createKinpersons() {
        Place krolowka = new Village("Krolowka", 102143);
        Kinperson florianKapcia = new Kinperson("Floarian Kapcia", 25, krolowka);
        Kinperson monikaKrajewska = new Kinperson("Monika Krajewska", 33, krolowka);
        Kinperson franciszekKapcia = new Kinperson("Stanislaw Kapcia", 73, krolowka);
        Kinperson mariaKapcia = new Kinperson("Maria Kapcia", 60, krolowka);

        List<Kinperson> krolowkaFamily = Arrays.asList(new Kinperson[]{florianKapcia, monikaKrajewska, franciszekKapcia, mariaKapcia});

        Place dabrowa = new Village("Dabrowa", 234233);
        Kinperson ryszardMichalik = new Kinperson("Ryszard Michalik", 45, dabrowa);
        Kinperson zuzannaMichalik = new Kinperson("Zuzanna Michalik", 39, dabrowa);
        Kinperson michalMichalik = new Kinperson("Michal Michalik", 16, dabrowa);
        Kinperson madziaMichalik = new Kinperson("Madzia Michalik", 19, dabrowa);

        List<Kinperson> dabrowaFamily = Arrays.asList(new Kinperson[]{ryszardMichalik, zuzannaMichalik, michalMichalik, madziaMichalik});

        Place bochnia = new Town("Bochnia", 30000);
        Kinperson stefanKapcia = new Kinperson("Stefan Kapcia", 65, bochnia);
        Kinperson helenaKapcia = new Kinperson("Helena Kapcia", 60, bochnia);
        Kinperson madziaWaskowicz = new Kinperson("Madzia Waskowicz", 40, bochnia);
        Kinperson aniaMalyszek = new Kinperson("Ania Malyszek", 38, bochnia);

        List<Kinperson> bochniaFamily = Arrays.asList(new Kinperson[]{stefanKapcia, helenaKapcia, madziaWaskowicz, aniaMalyszek});

        Place nowySacz = new Town("Nowy Sacz", 70000);
        Kinperson tadeuszRebilas = new Kinperson("Tadeusz Rebilas", 63, nowySacz);
        Kinperson ciocia = new Kinperson("ciocia", 62, nowySacz);
        Kinperson malgorzataRebilas = new Kinperson("Malgorzata Rebilas", 29, nowySacz);
        Kinperson katarzynaRebilas = new Kinperson("Katarzyna Rebilas", 33, nowySacz);
        Kinperson iwonaRebilasZawierta = new Kinperson("Iwona Rebilas-Zawierta", 39, nowySacz);

        List<Kinperson> nowySaczFamily = Arrays.asList(new Kinperson[]{tadeuszRebilas, ciocia, malgorzataRebilas, katarzynaRebilas, iwonaRebilasZawierta});

//        printAllInformation(krolowka, krolowkaFamily, dabrowa, dabrowaFamily, bochnia, bochniaFamily, nowySacz, nowySaczFamily);

        KinpersonDAO kinpersonDAO = new KinpersonDAOJDBCImpl();
        try {
            kinpersonDAO.create(tadeuszRebilas);
            kinpersonDAO.create(malgorzataRebilas);
            kinpersonDAO.create(florianKapcia);
            kinpersonDAO.create(madziaMichalik);
        } catch (KinpersonDAOException e) {
            e.printStackTrace();
        }
    }
}
