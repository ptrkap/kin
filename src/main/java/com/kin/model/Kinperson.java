package com.kin.model;

/**
 * Created by PK on 10/5/2015.
 */
public class Kinperson {

    private int id;
    private String name;
    private int age;
    private Place place;

    public Kinperson(String name, int age, Place place) {
        this.name = name;
        this.age = age;
        this.place = place;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    @Override
    public String toString() {
        return "KinPerson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", place=" + place +
                '}';
    }
}
