package com.kin.model;

import javax.persistence.*;

/**
 * Created by PK on 10/5/2015.
 */
@Entity
@Table(name = "Kinperson")
public class Kinperson3 {

    @Id
    @GeneratedValue
    @Column(name = "kinperson_id")
    private int id;
    private String name;
    private int age;
    @Column(name = "place_id")
    private int placeId;

    public Kinperson3(Integer id, String name, Integer age, Integer placeId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.placeId = placeId;
    }

    public Kinperson3() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPlace() {
        return placeId;
    }

    public void setPlace(int placeId) {
        this.placeId = placeId;
    }

    @Override
    public String toString() {
        return "KinPerson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", placeId=" + placeId +
                '}';
    }
}
