package com.kin.model;

/**
 * Created by PK on 10/5/2015.
 */
public class Kinperson2 {

    private int id;
    private String name;
    private int age;
    private int placeId;

    public Kinperson2(Integer id, String name, Integer age, Integer placeId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.placeId = placeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPlace() {
        return placeId;
    }

    public void setPlace(int placeId) {
        this.placeId = placeId;
    }

    @Override
    public String toString() {
        return "KinPerson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", placeId=" + placeId +
                '}';
    }
}
