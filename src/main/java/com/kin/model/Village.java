package com.kin.model;

/**
 * Created by PK on 10/5/2015.
 */
public class Village extends Place{

    private int fieldSize;

    public Village(String name, int fieldArea) {
        super(name);
        this.fieldSize = fieldArea;
    }

    public int getFieldSize() {
        return fieldSize;
    }

    public void setFieldSize(int fieldSize) {
        this.fieldSize = fieldSize;
    }

    @Override
    public String toString() {
        return "Village{" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "fieldSize=" + fieldSize +
                '}';
    }
}
