package com.kin.model;

/**
 * Created by PK on 10/5/2015.
 */
public class Town extends Place {

    private int numberOfPeople;

    public Town(String name, int numberOfPeople) {
        super(name);
        this.numberOfPeople = numberOfPeople;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    @Override
    public String toString() {
        return "Town{" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "numberOfPeople=" + numberOfPeople +
                '}';
    }
}
