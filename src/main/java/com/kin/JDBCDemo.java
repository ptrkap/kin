package com.kin;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import com.kin.dao.KinpersonDAO;
import com.kin.dao.exception.KinpersonDAOException;
import com.kin.dao.exception.TownDAOException;
import com.kin.dao.exception.VillageDAOException;
import com.kin.dao.impl.jdbc.KinpersonDAOJDBCImpl;
import com.kin.model.Kinperson;
import com.kin.model.Place;
import com.kin.model.Town;
import com.kin.model.Village;
import com.kin.dao.TownDAO;
import com.kin.dao.VillageDAO;
import com.kin.dao.impl.jdbc.TownDAOJDBCImpl;
import com.kin.dao.impl.jdbc.VillageDAOJDBCImpl;

import java.util.Arrays;
import java.util.List;

/**
 * Created by PK on 10/5/2015.
 */
public class JDBCDemo {

    public static void main(String[] args) {
//        createPlaces();
//        updatePlaces();
//        deletePlaces();
//        findPlacesByName();
//        findPlacesById();

        createKinpersons();
//        updateKinpersons();
//        deleteKinpersons();
//        findKinpersonsByName();
//        findKinpersonsById();
    }

    private static void findKinpersonsById() {
        KinpersonDAO kinpersonDAO = new KinpersonDAOJDBCImpl();
        try {
            System.out.println(kinpersonDAO.findById(9));
        } catch (KinpersonDAOException e) {
            e.printStackTrace();
        }
    }

    private static void findKinpersonsByName() {
        KinpersonDAO kinpersonDAO = new KinpersonDAOJDBCImpl();
        try {
            System.out.println(Arrays.toString(kinpersonDAO.findByName("Tadeusz Rebilas")));
        } catch (KinpersonDAOException e) {
            e.printStackTrace();
        }
    }

    private static void deleteKinpersons() {
        KinpersonDAO kinpersonDAO = new KinpersonDAOJDBCImpl();
        try {
            kinpersonDAO.delete(6);
        } catch (KinpersonDAOException e) {
            e.printStackTrace();
        }
    }

    private static void updateKinpersons() {
        Place bochnia = new Town("Bochnia", 30000);
        Kinperson stefanKapcia = new Kinperson("Stefan Kapcia", 65, bochnia);
        Kinperson helenaKapcia = new Kinperson("Helena Kapcia", 60, bochnia);
        Kinperson madziaWaskowicz = new Kinperson("Madzia Waskowicz", 40, bochnia);
        Kinperson aniaMalyszek = new Kinperson("Ania Malyszek", 38, bochnia);

        Place krolowka = new Village("Krolowka", 102143);
        Kinperson florianKapcia = new Kinperson("Floarian Kapcia", 25, krolowka);
        Kinperson monikaKrajewska = new Kinperson("Monika Krajewska", 33, krolowka);
        Kinperson franciszekKapcia = new Kinperson("Stanislaw Kapcia", 73, krolowka);
        Kinperson mariaKapcia = new Kinperson("Maria Kapcia", 60, krolowka);

        KinpersonDAO kinpersonDAO = new KinpersonDAOJDBCImpl();
        try {
            kinpersonDAO.update(9, aniaMalyszek);
        } catch (KinpersonDAOException e) {
            e.printStackTrace();
        }
    }

    private static void createKinpersons() {
        Place krolowka = new Village("Krolowka", 102143);
        Kinperson florianKapcia = new Kinperson("Floarian Kapcia", 25, krolowka);
        Kinperson monikaKrajewska = new Kinperson("Monika Krajewska", 33, krolowka);
        Kinperson franciszekKapcia = new Kinperson("Stanislaw Kapcia", 73, krolowka);
        Kinperson mariaKapcia = new Kinperson("Maria Kapcia", 60, krolowka);

        List<Kinperson> krolowkaFamily = Arrays.asList(new Kinperson[]{florianKapcia, monikaKrajewska, franciszekKapcia, mariaKapcia});

        Place dabrowa = new Village("Dabrowa", 234233);
        Kinperson ryszardMichalik = new Kinperson("Ryszard Michalik", 45, dabrowa);
        Kinperson zuzannaMichalik = new Kinperson("Zuzanna Michalik", 39, dabrowa);
        Kinperson michalMichalik = new Kinperson("Michal Michalik", 16, dabrowa);
        Kinperson madziaMichalik = new Kinperson("Madzia Michalik", 19, dabrowa);

        List<Kinperson> dabrowaFamily = Arrays.asList(new Kinperson[]{ryszardMichalik, zuzannaMichalik, michalMichalik, madziaMichalik});

        Place bochnia = new Town("Bochnia", 30000);
        Kinperson stefanKapcia = new Kinperson("Stefan Kapcia", 65, bochnia);
        Kinperson helenaKapcia = new Kinperson("Helena Kapcia", 60, bochnia);
        Kinperson madziaWaskowicz = new Kinperson("Madzia Waskowicz", 40, bochnia);
        Kinperson aniaMalyszek = new Kinperson("Ania Malyszek", 38, bochnia);

        List<Kinperson> bochniaFamily = Arrays.asList(new Kinperson[]{stefanKapcia, helenaKapcia, madziaWaskowicz, aniaMalyszek});

        Place nowySacz = new Town("Nowy Sacz", 70000);
        Kinperson tadeuszRebilas = new Kinperson("Tadeusz Rebilas", 63, nowySacz);
        Kinperson ciocia = new Kinperson("ciocia", 62, nowySacz);
        Kinperson malgorzataRebilas = new Kinperson("Malgorzata Rebilas", 29, nowySacz);
        Kinperson katarzynaRebilas = new Kinperson("Katarzyna Rebilas", 33, nowySacz);
        Kinperson iwonaRebilasZawierta = new Kinperson("Iwona Rebilas-Zawierta", 39, nowySacz);

        List<Kinperson> nowySaczFamily = Arrays.asList(new Kinperson[]{tadeuszRebilas, ciocia, malgorzataRebilas, katarzynaRebilas, iwonaRebilasZawierta});

//        printAllInformation(krolowka, krolowkaFamily, dabrowa, dabrowaFamily, bochnia, bochniaFamily, nowySacz, nowySaczFamily);

        KinpersonDAO kinpersonDAO = new KinpersonDAOJDBCImpl();
        try {
            kinpersonDAO.create(tadeuszRebilas);
            kinpersonDAO.create(malgorzataRebilas);
            kinpersonDAO.create(florianKapcia);
            kinpersonDAO.create(madziaMichalik);
        } catch (KinpersonDAOException e) {
            e.printStackTrace();
        }
    }

    private static void findPlacesById() {
        VillageDAO villageDAO = new VillageDAOJDBCImpl();
        TownDAO townDAO = new TownDAOJDBCImpl();
        try {
            System.out.println(villageDAO.findById(7));
            System.out.println(villageDAO.findById(8));
            System.out.println(townDAO.findById(5));
            System.out.println(townDAO.findById(6));
        } catch (VillageDAOException | TownDAOException e) {
            e.printStackTrace();
        }
    }

    private static void findPlacesByName() {
        VillageDAO villageDAO = new VillageDAOJDBCImpl();
        TownDAO townDAO = new TownDAOJDBCImpl();
        try {
            System.out.println(Arrays.toString(villageDAO.findByName("Stara Wies")));
            System.out.println(Arrays.toString(villageDAO.findByName("Nowa Wies 2")));
            System.out.println(Arrays.toString(townDAO.findByName("Bochnia 3")));
            System.out.println(Arrays.toString(townDAO.findByName("Nowy Sacz")));
        } catch (VillageDAOException | TownDAOException e) {
            e.printStackTrace();
        }
    }

    private static void deletePlaces() {
        VillageDAO villageDAO = new VillageDAOJDBCImpl();
        TownDAO townDAO = new TownDAOJDBCImpl();
        try {
            villageDAO.delete(5);
            townDAO.delete(8);
        } catch (VillageDAOException | TownDAOException e) {
            e.printStackTrace();
        }
    }

    private static void updatePlaces() {
        VillageDAO villageDAO = new VillageDAOJDBCImpl();
        TownDAO townDAO = new TownDAOJDBCImpl();
        try {
            villageDAO.update(13, new Village("Stara Wies 3", 322222));
            villageDAO.update(14, new Village("Nowa Wies 3", 333333));
            townDAO.update(15, new Town("Bochnia 3", 344444));
            townDAO.update(16, new Town("Nowy Sacz 3", 35555));
        } catch (VillageDAOException | TownDAOException e) {
            e.printStackTrace();
        }
    }

    private static void createPlaces() {
        List<Place> places = Arrays.asList(new Place[]{
                new Village("Stara Wies", 21344231),
                new Village("Nowa Wies", 713211),
                new Town("Krakow", 100000),
                new Town("Warszawa", 200000)
        });

        final VillageDAO villageDAO = new VillageDAOJDBCImpl();
        final TownDAO townDAO = new TownDAOJDBCImpl();

        places.forEach(place -> {
            if (place instanceof Village) {
                try {
                    villageDAO.create((Village) place);
                } catch (VillageDAOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    townDAO.create((Town) place);
                } catch (TownDAOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static void printAllInformation(Place krolowka, List<Kinperson> krolowkaFamily, Place dabrowa, List<Kinperson> dabrowaFamily, Place bochnia, List<Kinperson> bochniaFamily, Place nowySacz, List<Kinperson> nowySaczFamily) {
        System.out.println(bochniaFamily);
        System.out.println(krolowkaFamily);
        System.out.println(dabrowaFamily);
        System.out.println(nowySaczFamily);

        Multimap<Place, Kinperson> kinPersonByPlace = LinkedListMultimap.create();
        kinPersonByPlace.putAll(bochnia, bochniaFamily);
        kinPersonByPlace.putAll(krolowka, krolowkaFamily);
        kinPersonByPlace.putAll(dabrowa, dabrowaFamily);
        kinPersonByPlace.putAll(nowySacz, nowySaczFamily);

        System.out.println(kinPersonByPlace);
    }
}
