package com.kin.dao.exception;

/**
 * Created by PK on 10/6/2015.
 */
public class KinpersonDAOException extends Exception {
    public KinpersonDAOException(Exception e) {
        super(e);
    }
}
