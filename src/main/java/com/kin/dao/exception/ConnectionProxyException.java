package com.kin.dao.exception;

import java.sql.SQLException;

/**
 * Created by PK on 10/6/2015.
 */
public class ConnectionProxyException extends Exception {
    public ConnectionProxyException(SQLException e) {
        super(e);
    }
}
