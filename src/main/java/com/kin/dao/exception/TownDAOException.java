package com.kin.dao.exception;

import java.sql.SQLException;

/**
 * Created by PK on 10/6/2015.
 */
public class TownDAOException extends Exception {
    public TownDAOException(Exception e) {
        super(e);
    }
}
