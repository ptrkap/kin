package com.kin.dao;

import com.kin.dao.exception.KinpersonDAOException;
import com.kin.model.Kinperson;
import com.kin.model.Kinperson2;
import com.kin.model.Kinperson3;

/**
 * Created by PK on 10/6/2015.
 */
public interface KinpersonDAO3 {
    void create(Kinperson3 kinperson) throws KinpersonDAOException;
    void update(int kinpersonId, Kinperson3 kinperson) throws KinpersonDAOException;
    void delete(int kinpersonId) throws KinpersonDAOException;
    Kinperson[] findByName(String name) throws KinpersonDAOException;
    Kinperson3 findById(int kinpersonId) throws KinpersonDAOException;
}
