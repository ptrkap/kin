package com.kin.dao;

import com.kin.dao.exception.TownDAOException;
import com.kin.model.Town;

/**
 * Created by PK on 10/6/2015.
 */
public interface TownDAO {
    void create(Town town) throws TownDAOException;
    void update(int placeId, Town town) throws TownDAOException;
    void delete(int placeId) throws TownDAOException;
    Town[] findByName(String name) throws TownDAOException;
    Town findById(int placeId) throws TownDAOException;
}
