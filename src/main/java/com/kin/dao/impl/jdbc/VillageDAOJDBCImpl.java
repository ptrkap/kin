package com.kin.dao.impl.jdbc;

import com.kin.dao.VillageDAO;
import com.kin.dao.exception.VillageDAOException;
import com.kin.model.Village;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PK on 10/6/2015.
 */
public class VillageDAOJDBCImpl implements VillageDAO {

    @Override
    public void create(Village village) throws VillageDAOException {
        Connection connection = null;
        PreparedStatement createPlace = null;
        PreparedStatement createVillage = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            createPlace = connection.prepareStatement("INSERT INTO Place (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            createPlace.setString(1, village.getName());
            createPlace.executeUpdate();
            ResultSet generatedKeys = createPlace.getGeneratedKeys();
            generatedKeys.next();
            int placeId = generatedKeys.getInt(1);

//            System.out.println(324/0);

            createVillage = connection.prepareStatement("INSERT INTO Village (field_size, place_id) VALUES (?, ?)");
            createVillage.setInt(1, village.getFieldSize());
            createVillage.setInt(2, placeId);
            createVillage.executeUpdate();

            connection.commit();
            System.out.println("Created " + village);
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new VillageDAOException(e);
            } catch (SQLException e1) {
                throw new VillageDAOException(e1);
            }
        } finally {
            try {
                if (createPlace != null) {
                    createPlace.close();
                }
                if (createVillage != null) {
                    createVillage.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new VillageDAOException(e);
            }
        }
    }

    @Override
    public void update(int placeId, Village village) throws VillageDAOException {
        Connection connection = null;
        PreparedStatement updatePlace = null;
        PreparedStatement updateVillage = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            updatePlace = connection.prepareStatement("UPDATE Place SET name=? WHERE place_id=?");
            updatePlace.setString(1, village.getName());
            updatePlace.setInt(2, placeId);
            updatePlace.executeUpdate();

//            System.out.println(324/0);

            updateVillage = connection.prepareStatement("UPDATE Village SET field_size=? WHERE place_id=?");
            updateVillage.setInt(1, village.getFieldSize());
            updateVillage.setInt(2, placeId);
            updateVillage.executeUpdate();

            connection.commit();
            System.out.println("Updated " + village);
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new VillageDAOException(e);
            } catch (SQLException e1) {
                throw new VillageDAOException(e1);
            }
        } finally {
            try {
                if (updatePlace != null) {
                    updatePlace.close();
                }
                if (updateVillage != null) {
                    updateVillage.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new VillageDAOException(e);
            }
        }
    }

    @Override
    public void delete(int villageId) throws VillageDAOException {
        Connection connection = null;
        PreparedStatement deletePlace = null;
        PreparedStatement deleteVillage = null;
        PreparedStatement findVillage = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            findVillage = connection.prepareStatement("SELECT * FROM Village WHERE village_id=?");
            findVillage.setInt(1, villageId);
            ResultSet findVillageResultSet = findVillage.executeQuery();
            if (!findVillageResultSet.next())
                return;
            int placeId = findVillageResultSet.getInt(3);

            deleteVillage = connection.prepareStatement("DELETE FROM Village WHERE village_id=?");
            deleteVillage.setInt(1, villageId);
            deleteVillage.executeUpdate();

            deletePlace = connection.prepareStatement("DELETE FROM Place WHERE place_id=?");
            deletePlace.setInt(1, placeId);
            deletePlace.executeUpdate();

            connection.commit();
            System.out.println("Deleted village "+ villageId);
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new VillageDAOException(e);
            } catch (SQLException e1) {
                throw new VillageDAOException(e1);
            }
        } finally {
            try {
                if (deletePlace != null) {
                    deletePlace.close();
                }
                if (deleteVillage != null) {
                    deleteVillage.close();
                }
                if (findVillage != null) {
                    findVillage.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new VillageDAOException(e);
            }
        }
    }

    @Override
    public Village[] findByName(String name) throws VillageDAOException {
        Village[] villages = null;

        Connection connection = null;
        PreparedStatement findPlace = null;
        PreparedStatement findVillage = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            findPlace = connection.prepareStatement("SELECT * FROM Place WHERE name=?");
            findPlace.setString(1, name);
            ResultSet placeResultSet = findPlace.executeQuery();

            List<String> names = new ArrayList<>();
            List<Integer> placeIds = new ArrayList<>();

            while (placeResultSet.next()) {
                placeIds.add(placeResultSet.getInt(1));
                names.add(placeResultSet.getString(2));
            }

            List<Integer> villageIds = new ArrayList<>();
            List<Integer> fieldSizes = new ArrayList<>();
            for (Integer placeId : placeIds) {
                findVillage = connection.prepareStatement("SELECT * FROM Village WHERE place_id=?");
                findVillage.setInt(1, placeId);
                ResultSet villageResultSet = findVillage.executeQuery();
                villageResultSet.next();
                villageIds.add(villageResultSet.getInt(1));
                fieldSizes.add(villageResultSet.getInt(2));
            }

            List<Village> villagesList = new ArrayList<>();
            for (int i = 0; i < placeIds.size(); i++) {
                Village village = new Village(names.get(i), fieldSizes.get(i));
                village.setId(villageIds.get(i));
                villagesList.add(village);
            }

            villages = new Village[villagesList.size()];
            for (int i = 0; i < villagesList.size(); i++) {
                villages[i] = villagesList.get(i);
            }

            connection.commit();
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new VillageDAOException(e);
            } catch (SQLException e1) {
                throw new VillageDAOException(e1);
            }
        } finally {
            try {
                if (findPlace != null) {
                    findPlace.close();
                }
                if (findVillage != null) {
                    findVillage.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new VillageDAOException(e);
            }
        }

        return villages;
    }

    @Override
    public Village findById(int villageId) throws VillageDAOException {
        Village village = null;

        Connection connection = null;
        PreparedStatement findVillage = null;
        PreparedStatement findPlace = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            findVillage = connection.prepareStatement("SELECT * FROM Village WHERE village_id=?");
            findVillage.setInt(1, villageId);
            ResultSet villageResultSet = findVillage.executeQuery();

            if (!villageResultSet.next())
                return null;

            int numberOfPeople = villageResultSet.getInt(2);
            int placeId = villageResultSet.getInt(3);

            findPlace = connection.prepareStatement("SELECT * FROM Place WHERE place_id=?");
            findPlace.setInt(1, placeId);
            ResultSet placeResultSet = findPlace.executeQuery();
            placeResultSet.next();
            String name = placeResultSet.getString(2);
            village = new Village(name, numberOfPeople);
            village.setId(villageId);

            connection.commit();
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new VillageDAOException(e);
            } catch (SQLException e1) {
                throw new VillageDAOException(e1);
            }
        } finally {
            try {
                if (findPlace != null) {
                    findPlace.close();
                }
                if (findPlace != null) {
                    findPlace.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new VillageDAOException(e);
            }
        }

        return village;
    }
}
