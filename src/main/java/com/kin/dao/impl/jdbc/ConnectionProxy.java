package com.kin.dao.impl.jdbc;

import com.kin.dao.exception.ConnectionProxyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by PK on 10/6/2015.
 */
public class ConnectionProxy {

    private static Logger LOGGER = LoggerFactory.getLogger(ConnectionProxy.class);

    static Connection getConnection() throws ConnectionProxyException {
        Properties kinDbProperties = new Properties();
        Properties kinDbHostProperties = new Properties();
        String host = null;
        try {
            kinDbHostProperties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("kin_db_host.properties"));
            host = kinDbHostProperties.getProperty("host");
            kinDbProperties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("kin_db_" + host + ".properties"));
        } catch (IOException e) {
            LOGGER.error(e.toString());
        }
        String user = kinDbProperties.getProperty("user");
        String password = kinDbProperties.getProperty("password");
        String port = kinDbProperties.getProperty("port");
        String schema = kinDbProperties.getProperty("schema");


        Properties credentials = new Properties();
        credentials.put("user", user);
        Connection connection = null;
        credentials.put("password", password);
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + schema, credentials);
        } catch (SQLException e) {
            throw new ConnectionProxyException(e);
        }
        return connection;
    }
}
