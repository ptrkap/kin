package com.kin.dao.impl.jdbc;

import com.kin.dao.KinpersonDAO;
import com.kin.model.Kinperson;
import com.kin.model.Town;
import com.kin.dao.exception.KinpersonDAOException;
import com.kin.model.Village;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PK on 10/6/2015.
 */
public class KinpersonDAOJDBCImpl implements KinpersonDAO {

    @Override
    public void create(Kinperson kinperson) throws KinpersonDAOException {
        Connection connection = null;
        PreparedStatement createPlace = null;
        PreparedStatement createKinperson = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            createPlace = connection.prepareStatement("INSERT INTO Place (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            createPlace.setString(1, kinperson.getPlace().getName());
            createPlace.executeUpdate();
            ResultSet generatedKeys = createPlace.getGeneratedKeys();
            generatedKeys.next();
            int placeId = generatedKeys.getInt(1);

//            System.out.println(324/0);

            createKinperson = connection.prepareStatement("INSERT INTO Kinperson (name, age, place_id) VALUES (?, ?, ?)");
            createKinperson.setString(1, kinperson.getName());
            createKinperson.setInt(2, kinperson.getAge());
            createKinperson.setInt(3, placeId);
            createKinperson.executeUpdate();

            if (kinperson.getPlace() instanceof Village) {
                createKinperson = connection.prepareStatement("INSERT INTO Village (field_size, place_id) VALUES (?, ?)");
                createKinperson.setInt(1, ((Village) kinperson.getPlace()).getFieldSize());
                createKinperson.setInt(2, placeId);
                createKinperson.executeUpdate();
            } else {
                createKinperson = connection.prepareStatement("INSERT INTO Town (number_of_people, place_id) VALUES (?, ?)");
                createKinperson.setInt(1, ((Town) kinperson.getPlace()).getNumberOfPeople());
                createKinperson.setInt(2, placeId);
                createKinperson.executeUpdate();
            }

            connection.commit();
            System.out.println("Created " + kinperson);
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new KinpersonDAOException(e);
            } catch (SQLException e1) {
                throw new KinpersonDAOException(e1);
            }
        } finally {
            try {
                if (createPlace != null) {
                    createPlace.close();
                }
                if (createKinperson != null) {
                    createKinperson.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new KinpersonDAOException(e);
            }
        }
    }

    @Override
    public void update(int kinpersonId, Kinperson kinperson) throws KinpersonDAOException {
        kinperson.setId(kinpersonId);

        Connection connection = null;
        PreparedStatement updatePlace = null;
        PreparedStatement updateKinperson = null;
        PreparedStatement findVillage = null;
        PreparedStatement findTown = null;
        PreparedStatement deleteVillage = null;
        PreparedStatement deleteTown = null;
        PreparedStatement createVillage = null;
        PreparedStatement createTown = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            updateKinperson = connection.prepareStatement("SELECT * FROM Kinperson WHERE kinperson_id=?");
            updateKinperson.setInt(1, kinpersonId);
            ResultSet findKinpersonResultSet = updateKinperson.executeQuery();
            if (!findKinpersonResultSet.next())
                return;
            int placeId = findKinpersonResultSet.getInt(4);
            kinperson.getPlace().setId(placeId);

            updatePlace = connection.prepareStatement("UPDATE Place SET name=? WHERE place_id=?");
            updatePlace.setString(1, kinperson.getPlace().getName());
            updatePlace.setInt(2, placeId);
            updatePlace.executeUpdate();

//            System.out.println(324/0);

            updateKinperson = connection.prepareStatement("UPDATE Kinperson SET name=?, age=? WHERE place_id=?");
            updateKinperson.setString(1, kinperson.getName());
            updateKinperson.setInt(2, kinperson.getAge());
            updateKinperson.setInt(3, placeId);
            updateKinperson.executeUpdate();

            if (kinperson.getPlace() instanceof Village) {
                // todo find by place id
                findVillage = connection.prepareStatement("SELECT * FROM Village WHERE place_id=?");
                findVillage.setInt(1, placeId);
                ResultSet findVillageResultSet = findVillage.executeQuery();
                if (findVillageResultSet.next()) {
                    updateKinperson = connection.prepareStatement("UPDATE Village SET field_size=? WHERE place_id=?");
                    updateKinperson.setInt(1, ((Village) kinperson.getPlace()).getFieldSize());
                    updateKinperson.setInt(2, placeId);
                    updateKinperson.executeUpdate();
                } else {
                    // todo delete Village and create Town
                    deleteVillage = connection.prepareStatement("DELETE FROM Town WHERE place_id=?");
                    deleteVillage.setInt(1, placeId);
                    deleteVillage.executeUpdate();

                    createTown = connection.prepareStatement("INSERT INTO Village (field_size, place_id) values (?, ?)");
                    createTown.setInt(1, ((Village) kinperson.getPlace()).getFieldSize());
                    createTown.setInt(2, placeId);
                    createTown.executeUpdate();
                }
            } else {
                // todo find by place id
                findTown = connection.prepareStatement("SELECT * FROM Town WHERE place_id=?");
                findTown.setInt(1, placeId);
                ResultSet findTownResultSet = findTown.executeQuery();
                if (findTownResultSet.next()) {
                    updateKinperson = connection.prepareStatement("UPDATE Town SET number_of_people=? WHERE place_id=?");
                    updateKinperson.setInt(1, ((Town) kinperson.getPlace()).getNumberOfPeople());
                    updateKinperson.setInt(2, placeId);
                    updateKinperson.executeUpdate();
                } else {
                    // todo delete Town and create Village
                    deleteTown = connection.prepareStatement("DELETE FROM Village WHERE place_id=?");
                    deleteTown.setInt(1, placeId);
                    deleteTown.executeUpdate();

                    createTown = connection.prepareStatement("INSERT INTO Town (number_of_people, place_id) values (?, ?)");
                    createTown.setInt(1, ((Town) kinperson.getPlace()).getNumberOfPeople());
                    createTown.setInt(2, placeId);
                    createTown.executeUpdate();
                }
            }

            connection.commit();
            System.out.println("Updated " + kinperson);
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new KinpersonDAOException(e);
            } catch (SQLException e1) {
                throw new KinpersonDAOException(e1);
            }
        } finally {
            try {
                if (updatePlace != null) {
                    updatePlace.close();
                }
                if (updateKinperson != null) {
                    updateKinperson.close();
                }
                if (findVillage != null) {
                    findVillage.close();
                }
                if (findTown != null) {
                    findTown.close();
                }
                if (deleteVillage != null) {
                    deleteVillage.close();
                }
                if (deleteTown != null) {
                    deleteTown.close();
                }
                if (createVillage != null) {
                    createVillage.close();
                }
                if (createTown != null) {
                    createTown.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new KinpersonDAOException(e);
            }
        }
    }

    @Override
    public void delete(int kinpersonId) throws KinpersonDAOException {
        Connection connection = null;
        PreparedStatement deletePlace = null;
        PreparedStatement deleteVillage = null;
        PreparedStatement deleteTown = null;
        PreparedStatement deleteKinperson = null;
        PreparedStatement findKinperson = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            findKinperson = connection.prepareStatement("SELECT * FROM Kinperson WHERE kinperson_id=?");
            findKinperson.setInt(1, kinpersonId);
            ResultSet findVillageResultSet = findKinperson.executeQuery();
            if (!findVillageResultSet.next())
                return;
            int placeId = findVillageResultSet.getInt(3);

            deleteKinperson = connection.prepareStatement("DELETE FROM Kinperson WHERE kinperson_id=?");
            deleteKinperson.setInt(1, kinpersonId);
            deleteKinperson.executeUpdate();

            deletePlace = connection.prepareStatement("DELETE FROM Place WHERE place_id=?");
            deletePlace.setInt(1, placeId);
            deletePlace.executeUpdate();

            deleteVillage = connection.prepareStatement("DELETE FROM Village WHERE place_id=?");
            deleteVillage.setInt(1, placeId);
            deleteVillage.executeUpdate();

            deleteTown = connection.prepareStatement("DELETE FROM Town WHERE place_id=?");
            deleteTown.setInt(1, placeId);
            deleteTown.executeUpdate();

            connection.commit();
            System.out.println("Deleted kinperson "+ kinpersonId);
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new KinpersonDAOException(e);
            } catch (SQLException e1) {
                throw new KinpersonDAOException(e1);
            }
        } finally {
            try {
                if (deletePlace != null) {
                    deletePlace.close();
                }
                if (deleteVillage != null) {
                    deleteVillage.close();
                }
                if (deleteTown != null) {
                    deleteTown.close();
                }
                if (deleteKinperson != null) {
                    deleteKinperson.close();
                }
                if (findKinperson != null) {
                    findKinperson.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new KinpersonDAOException(e);
            }
        }
    }

    @Override
    public Kinperson[] findByName(String name) throws KinpersonDAOException {
        Kinperson[] kinpersons = null;

        Connection connection = null;
        PreparedStatement findPlace = null;
        PreparedStatement findVillage = null;
        PreparedStatement findTown = null;
        PreparedStatement findKinperson = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            findKinperson = connection.prepareStatement("SELECT * FROM Kinperson WHERE name=?");
            findKinperson.setString(1, name);
            ResultSet kinpersonResultSet = findKinperson.executeQuery();

            List<Integer> kinpersonIds = new ArrayList<>();
            List<String> names = new ArrayList<>();
            List<Integer> ages = new ArrayList<>();
            List<Integer> placeIds = new ArrayList<>();

            while (kinpersonResultSet.next()) {
                kinpersonIds.add(kinpersonResultSet.getInt(1));
                names.add(kinpersonResultSet.getString(2));
                ages.add(kinpersonResultSet.getInt(3));
                placeIds.add(kinpersonResultSet.getInt(4));
            }

            List<String> placeNames = new ArrayList<>();
            for (Integer placeId : placeIds) {
                findPlace = connection.prepareStatement("SELECT * FROM Place WHERE place_id=?");
                findPlace.setInt(1, placeId);
                ResultSet placeResultSet = findPlace.executeQuery();
                placeResultSet.next();
//                placeIds.add(placeResultSet.getInt(1));
                placeNames.add(placeResultSet.getString(2));
            }

            List<Integer> villageIds = new ArrayList<>();
            List<Integer> fieldSizes = new ArrayList<>();
            for (Integer placeId : placeIds) {
                findVillage = connection.prepareStatement("SELECT * FROM Village WHERE place_id=?");
                findVillage.setInt(1, placeId);
                ResultSet villageResultSet = findVillage.executeQuery();
                if (villageResultSet.next()) {
                    villageIds.add(villageResultSet.getInt(1));
                    fieldSizes.add(villageResultSet.getInt(2));
                } else {
                    villageIds.add(null);
                    fieldSizes.add(null);
                }
            }

            List<Integer> townIds = new ArrayList<>();
            List<Integer> numberOfPeople = new ArrayList<>();
            for (Integer placeId : placeIds) {
                findTown = connection.prepareStatement("SELECT * FROM Town WHERE place_id=?");
                findTown.setInt(1, placeId);
                ResultSet townResultSet = findTown.executeQuery();
                if (townResultSet.next()) {
                    townIds.add(townResultSet.getInt(1));
                    numberOfPeople.add(townResultSet.getInt(2));
                } else {
                    townIds.add(null);
                    numberOfPeople.add(null);
                }
            }

            List<Village> villagesList = new ArrayList<>();
            for (int i = 0; i < kinpersonIds.size(); i++) {
                if (villageIds.get(i) == null) {
                    villagesList.add(null);
                } else {
                    Village village = new Village(placeNames.get(i), fieldSizes.get(i));
                    village.setId(villageIds.get(i));
                    villagesList.add(village);
                }
            }

            List<Town> townList = new ArrayList<>();
            for (int i = 0; i < kinpersonIds.size(); i++) {
                if (townIds.get(i) == null) {
                    townList.add(null);
                } else {
                    Town town = new Town(placeNames.get(i), numberOfPeople.get(i));
                    town.setId(townIds.get(i));
                    townList.add(town);
                }
            }

            kinpersons = new Kinperson[kinpersonIds.size()];
            for (int i = 0; i < villagesList.size(); i++) {
                if (villagesList.get(i) != null) {
                    Village place = new Village(placeNames.get(i), fieldSizes.get(i));
                    place.setId(villageIds.get(i));
                    Kinperson kinperson = new Kinperson(names.get(i), ages.get(i), place);
                    kinperson.setId(kinpersonIds.get(i));
                    kinpersons[i] = kinperson;
                } else {
                    Town place = new Town(placeNames.get(i), numberOfPeople.get(i));
                    place.setId(townIds.get(i));
                    Kinperson kinperson = new Kinperson(names.get(i), ages.get(i), place);
                    kinperson.setId(kinpersonIds.get(i));
                    kinpersons[i] = kinperson;
                }
            }

            connection.commit();
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new KinpersonDAOException(e);
            } catch (SQLException e1) {
                throw new KinpersonDAOException(e1);
            }
        } finally {
            try {
                if (findPlace != null) {
                    findPlace.close();
                }
                if (findKinperson != null) {
                    findKinperson.close();
                }
                if (findVillage != null) {
                    findVillage.close();
                }
                if (findTown != null) {
                    findTown.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new KinpersonDAOException(e);
            }
        }

        return kinpersons;
    }

    @Override
    public Kinperson findById(int kinpersonId) throws KinpersonDAOException {
        Kinperson kinperson = null;

        Connection connection = null;
        PreparedStatement findKinperson = null;
        PreparedStatement findPlace = null;
        PreparedStatement findTown = null;
        PreparedStatement findVillage = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            findKinperson = connection.prepareStatement("SELECT * FROM Kinperson WHERE kinperson_id=?");
            findKinperson.setInt(1, kinpersonId);
            ResultSet kinpersonResultSet = findKinperson.executeQuery();

            if (!kinpersonResultSet.next())
                return null;

            String name = kinpersonResultSet.getString(2);
            int age = kinpersonResultSet.getInt(3);
            int placeId = kinpersonResultSet.getInt(4);

            findPlace = connection.prepareStatement("SELECT * FROM Place WHERE place_id=?");
            findPlace.setInt(1, placeId);
            ResultSet placeResultSet = findPlace.executeQuery();
            placeResultSet.next();
            String placeName = placeResultSet.getString(2);

            kinperson = createKinperson(connection, findVillage, findTown, placeId, name, age, placeName);
            kinperson.setId(kinpersonId);

            connection.commit();
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new KinpersonDAOException(e);
            } catch (SQLException e1) {
                throw new KinpersonDAOException(e1);
            }
        } finally {
            try {
                if (findPlace != null) {
                    findPlace.close();
                }
                if (findPlace != null) {
                    findPlace.close();
                }
                if (findVillage != null) {
                    findVillage.close();
                }
                if (findTown != null) {
                    findTown.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new KinpersonDAOException(e);
            }
        }

        return kinperson;
    }

    private Kinperson createKinperson(Connection connection, PreparedStatement findVillage, PreparedStatement findTown, int placeId, String name, int age, String placeName) throws SQLException {
        findVillage = connection.prepareStatement("SELECT * FROM Village WHERE place_id=?");
        findVillage.setInt(1, placeId);
        ResultSet villageResultSet = findVillage.executeQuery();
        if (villageResultSet.next()) {
            int villageId = villageResultSet.getInt(1);
            int fieldSize = villageResultSet.getInt(2);
            Village place = new Village(placeName, fieldSize);
            place.setId(villageId);
            return new Kinperson(name, age, place);
        } else {
            findTown = connection.prepareStatement("SELECT * FROM Town WHERE place_id=?");
            findTown.setInt(1, placeId);
            ResultSet townResultSet = findTown.executeQuery();
            townResultSet.next();
            int townId = townResultSet.getInt(1);
            int numberOfPeople = townResultSet.getInt(2);
            Town place = new Town(placeName, numberOfPeople);
            place.setId(townId);
            return new Kinperson(name, age, place);
        }
    }
}
