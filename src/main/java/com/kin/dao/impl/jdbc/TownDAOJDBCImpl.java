package com.kin.dao.impl.jdbc;

import com.kin.dao.TownDAO;
import com.kin.dao.exception.TownDAOException;
import com.kin.model.Town;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PK on 10/6/2015.
 */
public class TownDAOJDBCImpl implements TownDAO {

    @Override
    public void create(Town town) throws TownDAOException {
        Connection connection = null;
        PreparedStatement createPlace = null;
        PreparedStatement createTown = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            createPlace = connection.prepareStatement("INSERT INTO Place (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            createPlace.setString(1, town.getName());
            createPlace.executeUpdate();
            ResultSet generatedKeys = createPlace.getGeneratedKeys();
            generatedKeys.next();
            int placeId = generatedKeys.getInt(1);

//            System.out.println(324/0);

            createTown = connection.prepareStatement("INSERT INTO Town (number_of_people, place_id) VALUES (?, ?)");
            createTown.setInt(1, town.getNumberOfPeople());
            createTown.setInt(2, placeId);
            createTown.executeUpdate();

            connection.commit();
            System.out.println("Created " + town);
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new TownDAOException(e);
            } catch (SQLException e1) {
                throw new TownDAOException(e1);
            }
        } finally {
            try {
                if (createPlace != null) {
                    createPlace.close();
                }
                if (createTown != null) {
                    createTown.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new TownDAOException(e);
            }
        }
    }

    @Override
    public void update(int placeId, Town town) throws TownDAOException {
        Connection connection = null;
        PreparedStatement updatePlace = null;
        PreparedStatement updateTown = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            updatePlace = connection.prepareStatement("UPDATE Place SET name=? WHERE place_id=?");
            updatePlace.setString(1, town.getName());
            updatePlace.setInt(2, placeId);
            updatePlace.executeUpdate();

//            System.out.println(324/0);

            updateTown = connection.prepareStatement("UPDATE Town SET number_of_people=? WHERE place_id=?");
            updateTown.setInt(1, town.getNumberOfPeople());
            updateTown.setInt(2, placeId);
            updateTown.executeUpdate();

            connection.commit();
            System.out.println("Updated " + town);
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new TownDAOException(e);
            } catch (SQLException e1) {
                throw new TownDAOException(e1);
            }
        } finally {
            try {
                if (updatePlace != null) {
                    updatePlace.close();
                }
                if (updateTown != null) {
                    updateTown.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new TownDAOException(e);
            }
        }
    }

    @Override
    public void delete(int townId) throws TownDAOException {
        Connection connection = null;
        PreparedStatement deletePlace = null;
        PreparedStatement deleteTown = null;
        PreparedStatement findTown = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            findTown = connection.prepareStatement("SELECT * FROM Town WHERE town_id=?");
            findTown.setInt(1, townId);
            ResultSet findTownResultSet = findTown.executeQuery();
            if (!findTownResultSet.next())
                return;
            int placeId = findTownResultSet.getInt(3);

            deleteTown = connection.prepareStatement("DELETE FROM Town WHERE town_id=?");
            deleteTown.setInt(1, townId);
            deleteTown.executeUpdate();

            deletePlace = connection.prepareStatement("DELETE FROM Place WHERE place_id=?");
            deletePlace.setInt(1, placeId);
            deletePlace.executeUpdate();

            connection.commit();
            System.out.println("Deleted town " + townId);
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new TownDAOException(e);
            } catch (SQLException e1) {
                throw new TownDAOException(e1);
            }
        } finally {
            try {
                if (deletePlace != null) {
                    deletePlace.close();
                }
                if (deleteTown != null) {
                    deleteTown.close();
                }
                if (findTown != null) {
                    findTown.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new TownDAOException(e);
            }
        }
    }

    @Override
    public Town[] findByName(String name) throws TownDAOException{
        Town[] towns = null;

        Connection connection = null;
        PreparedStatement findPlace = null;
        PreparedStatement findTown = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            findPlace = connection.prepareStatement("SELECT * FROM Place WHERE name=?");
            findPlace.setString(1, name);
            ResultSet placeResultSet = findPlace.executeQuery();

            List<String> names = new ArrayList<>();
            List<Integer> placeIds = new ArrayList<>();

            while (placeResultSet.next()) {
                placeIds.add(placeResultSet.getInt(1));
                names.add(placeResultSet.getString(2));
            }

            List<Integer> townIds = new ArrayList<>();
            List<Integer> fieldSizes = new ArrayList<>();
            for (Integer placeId : placeIds) {
                findTown = connection.prepareStatement("SELECT * FROM Town WHERE place_id=?");
                findTown.setInt(1, placeId);
                ResultSet townResultSet = findTown.executeQuery();
                townResultSet.next();
                townIds.add(townResultSet.getInt(1));
                fieldSizes.add(townResultSet.getInt(2));
            }

            List<Town> townsList = new ArrayList<>();
            for (int i = 0; i < placeIds.size(); i++) {
                Town town = new Town(names.get(i), fieldSizes.get(i));
                town.setId(townIds.get(i));
                townsList.add(town);
            }

            towns = new Town[townsList.size()];
            for (int i = 0; i < townsList.size(); i++) {
                towns[i] = townsList.get(i);
            }

            connection.commit();
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new TownDAOException(e);
            } catch (SQLException e1) {
                throw new TownDAOException(e1);
            }
        } finally {
            try {
                if (findPlace != null) {
                    findPlace.close();
                }
                if (findTown != null) {
                    findTown.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new TownDAOException(e);
            }
        }

        return towns;
    }

    @Override
    public Town findById(int townId) throws TownDAOException {
        Town town = null;

        Connection connection = null;
        PreparedStatement findTown = null;
        PreparedStatement findPlace = null;
        try {
            connection = ConnectionProxy.getConnection();
            connection.setAutoCommit(false);

            findTown = connection.prepareStatement("SELECT * FROM Town WHERE town_id=?");
            findTown.setInt(1, townId);
            ResultSet townResultSet = findTown.executeQuery();

            if (!townResultSet.next())
                return null;

            int numberOfPeople = townResultSet.getInt(2);
            int placeId = townResultSet.getInt(3);

            findPlace = connection.prepareStatement("SELECT * FROM Place WHERE place_id=?");
            findPlace.setInt(1, placeId);
            ResultSet placeResultSet = findPlace.executeQuery();
            placeResultSet.next();
            String name = placeResultSet.getString(2);
            town = new Town(name, numberOfPeople);
            town.setId(townId);

            connection.commit();
        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    System.out.println("Rolled back");
                }
                throw new TownDAOException(e);
            } catch (SQLException e1) {
                throw new TownDAOException(e1);
            }
        } finally {
            try {
                if (findPlace != null) {
                    findPlace.close();
                }
                if (findPlace != null) {
                    findPlace.close();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                throw new TownDAOException(e);
            }
        }

        return town;
    }
}
