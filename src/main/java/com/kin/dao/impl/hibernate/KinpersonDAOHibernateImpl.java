package com.kin.dao.impl.hibernate;

import com.kin.dao.KinpersonDAO3;
import com.kin.dao.exception.KinpersonDAOException;
import com.kin.model.Kinperson;
import com.kin.model.Kinperson3;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 * Created by PK on 10/7/2015.
 */
public class KinpersonDAOHibernateImpl implements KinpersonDAO3 {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Kinperson3 kinperson) throws KinpersonDAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(kinperson);
        tx.commit();
        session.close();
    }

    @Override
    public void update(int kinpersonId, Kinperson3 kinperson) throws KinpersonDAOException {

    }

    @Override
    public void delete(int kinpersonId) throws KinpersonDAOException {

    }

    @Override
    public Kinperson[] findByName(String name) throws KinpersonDAOException {
        return new Kinperson[0];
    }

    @Override
    public Kinperson3 findById(int kinpersonId) throws KinpersonDAOException {
        Session session = this.sessionFactory.openSession();
        Kinperson3 kinperson = (Kinperson3) session.createQuery("from Kinperson3 where id='" + kinpersonId + "'").uniqueResult();

        session.close();
        return kinperson;
    }
}
