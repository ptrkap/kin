package com.kin.dao.impl.mybatis;

import com.kin.dao.KinpersonDAO;
import com.kin.dao.KinpersonDAO2;
import com.kin.dao.exception.KinpersonDAOException;
import com.kin.mapper.KinpersonMapper;
import com.kin.model.Kinperson;
import com.kin.model.Kinperson2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by PK on 10/7/2015.
 */
@Service
public class KinpersonDAOMybatisImpl implements KinpersonDAO2 {

    @Autowired
    KinpersonMapper kinpersonMapper;

    @Override
    public void create(Kinperson kinperson) throws KinpersonDAOException {

    }

    @Override
    public void update(int kinpersonId, Kinperson kinperson) throws KinpersonDAOException {

    }

    @Override
    public void delete(int kinpersonId) throws KinpersonDAOException {

    }

    @Override
    public Kinperson[] findByName(String name) throws KinpersonDAOException {
        return new Kinperson[0];
    }

    @Override
    public Kinperson2 findById(int kinpersonId) throws KinpersonDAOException {
        return kinpersonMapper.getById(kinpersonId);
    }
}
