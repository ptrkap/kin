package com.kin.dao;

import com.kin.model.Kinperson;
import com.kin.dao.exception.KinpersonDAOException;

/**
 * Created by PK on 10/6/2015.
 */
public interface KinpersonDAO {
    void create(Kinperson kinperson) throws KinpersonDAOException;
    void update(int kinpersonId, Kinperson kinperson) throws KinpersonDAOException;
    void delete(int kinpersonId) throws KinpersonDAOException;
    Kinperson[] findByName(String name) throws KinpersonDAOException;
    Kinperson findById(int kinpersonId) throws KinpersonDAOException;
}
