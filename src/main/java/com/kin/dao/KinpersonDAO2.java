package com.kin.dao;

import com.kin.dao.exception.KinpersonDAOException;
import com.kin.model.Kinperson;
import com.kin.model.Kinperson2;

/**
 * Created by PK on 10/6/2015.
 */
public interface KinpersonDAO2 {
    void create(Kinperson kinperson) throws KinpersonDAOException;
    void update(int kinpersonId, Kinperson kinperson) throws KinpersonDAOException;
    void delete(int kinpersonId) throws KinpersonDAOException;
    Kinperson[] findByName(String name) throws KinpersonDAOException;
    Kinperson2 findById(int kinpersonId) throws KinpersonDAOException;
}
