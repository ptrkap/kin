package com.kin.dao;

import com.kin.dao.exception.VillageDAOException;
import com.kin.model.Village;

/**
 * Created by PK on 10/6/2015.
 */
public interface VillageDAO {
    void create(Village village) throws VillageDAOException;
    void update(int placeId, Village village) throws VillageDAOException;
    void delete(int placeId) throws VillageDAOException;
    Village[] findByName(String name) throws VillageDAOException;
    Village findById(int placeId) throws VillageDAOException;
}
