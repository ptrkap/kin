package com.kin.mapper;

import com.kin.model.Kinperson;
import com.kin.model.Kinperson2;
import org.apache.ibatis.annotations.Select;

/**
 * Created by PK on 10/7/2015.
 */
public interface KinpersonMapper {

    @Select("SELECT * FROM Kinperson WHERE kinperson_id=#{kinpersonId}")
    Kinperson2 getById(int kinpersonId);
}
